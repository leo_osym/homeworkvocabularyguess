package com.example.homeworkvocabularyguess

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private val listOfTranslations = ArrayList<String>()
    private val russianSet = ArrayList<String>()
    private val sindarinSet = ArrayList<String>()

    private var currentRusWord = String()
    private var currentSindWord = String()
    private lateinit var myAdapter : ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        readDictionaryFile()
        setupListView()

        definitions_list.setOnItemClickListener{ _,view,index,_ ->
            var selectedWord = (view as TextView).getText().toString()
            if(selectedWord == currentRusWord){
                Toast.makeText(this, "Correct!", Toast.LENGTH_SHORT).show()
                view.setTextColor(Color.RED)
                the_word.text = "$currentSindWord (Click here to continue)"
            }
            else {
                Toast.makeText(this, "Wrong!", Toast.LENGTH_SHORT).show()
                listOfTranslations.removeAt(index)
                myAdapter.notifyDataSetChanged()
            }

        }

        the_word.setOnClickListener {
            setupListView()
        }

    }

    // In file line with sindarin goes after line with russian
    // so I sorted it into two different lists step-by-step
    fun readDictionaryFile(){
        var index = true
        val reader = Scanner(resources.openRawResource(R.raw.sindarin), "utf-8")
        while(reader.hasNextLine()){
            val line = reader.nextLine()
            if(index){
                russianSet.add(line)
                index = false
            } else {
                sindarinSet.add(line)
                index = true
            }
        }
    }

    private fun setupListView(){

        val random = Random()
        var index = random.nextInt(sindarinSet.size)
        currentSindWord = sindarinSet[index]
        currentRusWord = russianSet[index]
        the_word.text = currentSindWord
        the_word.setTextColor(Color.BLUE)

        listOfTranslations.clear()
        listOfTranslations.add(currentRusWord)
        for(i in 0..9){
            listOfTranslations.add(russianSet[random.nextInt(russianSet.size)])
        }
        listOfTranslations.shuffle()
        myAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listOfTranslations)

        definitions_list.adapter = myAdapter
    }
}

